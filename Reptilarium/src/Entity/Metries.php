<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MetriesRepository")
 */
class Metries
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Poids;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Taille;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Temperature;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Hygrometrie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="SerpentMetries")
     */
    private $serpents;

    public function __construct()
    {
        $this->serpents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPoids(): ?int
    {
        return $this->Poids;
    }

    public function setPoids(?int $Poids): self
    {
        $this->Poids = $Poids;

        return $this;
    }

    public function getTaille(): ?int
    {
        return $this->Taille;
    }

    public function setTaille(?int $Taille): self
    {
        $this->Taille = $Taille;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getTemperature(): ?int
    {
        return $this->Temperature;
    }

    public function setTemperature(?int $Temperature): self
    {
        $this->Temperature = $Temperature;

        return $this;
    }

    public function getHygrometrie(): ?int
    {
        return $this->Hygrometrie;
    }

    public function setHygrometrie(?int $Hygrometrie): self
    {
        $this->Hygrometrie = $Hygrometrie;

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getSerpents(): Collection
    {
        return $this->serpents;
    }

    public function addSerpent(Serpent $serpent): self
    {
        if (!$this->serpents->contains($serpent)) {
            $this->serpents[] = $serpent;
            $serpent->setSerpentMetries($this);
        }

        return $this;
    }

    public function removeSerpent(Serpent $serpent): self
    {
        if ($this->serpents->contains($serpent)) {
            $this->serpents->removeElement($serpent);
            // set the owning side to null (unless already changed)
            if ($serpent->getSerpentMetries() === $this) {
                $serpent->setSerpentMetries(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "blabla";
    }
}
