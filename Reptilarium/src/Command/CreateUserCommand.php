<?php

namespace App\Command;

use App\Entity\Terrariophile;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateUserCommand extends Command
{
    private $entityManager;
    public function __construct(string $name = null , EntityManagerInterface $entityManager)
    {
        parent::__construct($name);
        $this->entityManager=$entityManager;
    }

    protected static $defaultName = 'app:create-terrariophile';

    protected function configure()
    {
        $this
            ->setDescription('Create a new terrariophile entity')
            ->addArgument('mail', InputArgument::OPTIONAL, 'Terrariophile mail')

//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $newterrariophile= new Terrariophile();
        $output->writeln('Create a new Terrariophile');
        $newterrariophile->setMail($input->getArgument('mail'));
        $prenom=$io->ask('Entrez le prénom du nouveau terrariophile.');
        $newterrariophile->setPrenom($prenom);
        $nom=$io->ask('Entrez le nom du nouveau terrariophile.');
        $newterrariophile->setNom($nom);
        $telephone=$io->ask('Entrez le numéro de téléphone du nouveau terrariophile.');
        $newterrariophile->setTelephone($telephone);
        $this->entityManager->persist($newterrariophile);
        $this->entityManager->flush();
         $output->writeln('Success');
//        $io = new SymfonyStyle($input, $output);
//        $arg1 = $input->getArgument('arg1');
//
//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }
//
//        if ($input->getOption('option1')) {
//            // ...
//        }
//
//        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
