<?php

namespace App\Controller;
use App\Entity\Terrariophile;
use App\Repository\TerrariophileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;




class SecurityController extends AbstractController
{





private $terrariophileRepository;
    public function __construct(TerrariophileRepository $terrariophileRepository)
    {
        $this->terrariophileRepository=$terrariophileRepository;
    }

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {



            if ($this->getUser()) {
                $this->redirectToRoute('page_accueil');
                $this->addFlash('notice', 'Bienvenue à toi !');
            }


            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();
            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();





            return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);

        }


    /**
     * @Route("/logout", name="app_logout" , methods={"GET"})
     */
    public function logout()
    {
        return $this->redirectToRoute('page_accueil');

//        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');

    }

}
