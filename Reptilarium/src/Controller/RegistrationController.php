<?php

namespace App\Controller;

use App\Entity\Terrariophile;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    private $mailer;
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer=$mailer;
    }

    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $nouveauTerrariophile = new Terrariophile();
        $form = $this->createForm(RegistrationFormType::class, $nouveauTerrariophile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $nouveauTerrariophile->setPassword(
                $passwordEncoder->encodePassword(
                    $nouveauTerrariophile,
                    $form->get('plainPassword')->getData()

                )

            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($nouveauTerrariophile);
            $entityManager->flush();


            // do anything else you need here, like send an email
//            $message= new \Swift_Message();
//            $message->setSubject('Oyé, oyé');
//            $message->setTo('m.martinmarchand@gmail.com');
//            $message->setFrom('monseigneurdesnains@yahoo.fr');
//
//
//
//            $message->setBody($this->renderView('/page_accueil/email-register.html.twig'), 'text/html');
//            $message->attach(\Swift_Attachment::fromPath('../public/img/Jones1.jpg'));
//            $this->mailer->send($message);

            $this->addFlash('notice','Bienvenue à toi !');
            return $this->redirectToRoute('page_accueil');

        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
