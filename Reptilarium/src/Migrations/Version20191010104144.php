<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191010104144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE metries ADD temperature INT DEFAULT NULL, ADD hygrometrie INT DEFAULT NULL, DROP température, DROP hygrométrie');
        $this->addSql('ALTER TABLE terrariophile CHANGE prénom prenom VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE serpent ADD espece VARCHAR(255) DEFAULT NULL, ADD caractere VARCHAR(255) DEFAULT NULL, DROP espèce, DROP caractère');
        $this->addSql('ALTER TABLE habitat ADD systeme_thermique VARCHAR(255) DEFAULT NULL, ADD systeme_hygrométrique VARCHAR(255) DEFAULT NULL, DROP système_thermique, DROP système_hygrométrique');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE habitat ADD système_thermique VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD système_hygrométrique VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP systeme_thermique, DROP systeme_hygrométrique');
        $this->addSql('ALTER TABLE metries ADD température INT DEFAULT NULL, ADD hygrométrie INT DEFAULT NULL, DROP temperature, DROP hygrometrie');
        $this->addSql('ALTER TABLE serpent ADD espèce VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD caractère VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, DROP espece, DROP caractere');
        $this->addSql('ALTER TABLE terrariophile CHANGE prenom prénom VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
