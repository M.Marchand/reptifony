var $collectionHolder;

// setup an "add a photo" link
var $addPhotoButton = $('<button type="button" class="add_photo_link">Add photo</button>');
var $newLinkDiv = $('<div></div>').append($addPhotoButton);

$(function() {
    // Get the ul that holds the collection of photos
    $collectionHolder = $('#new_terrariophile_photos');

    // add a delete link to all of the existing tag form div elements
    $collectionHolder.find('> div').each(function() {
        if($(this).html()) {
            addPhotoFormDeleteLink($(this));
        }
    });


    // add the "add a photo" anchor and div to the photos ul
    $collectionHolder.append($newLinkDiv);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addPhotoButton.on('click', function(e) {
        // add a new photo form (see next code block)
        addPhotoForm($collectionHolder, $newLinkDiv);
    });
});
function addPhotoForm($collectionHolder, $newLinkDiv) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // You need this only if you didn't set 'label' => false in your photos field in TaskType
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an div, before the "Add a photo" link div
    var $newFormDiv = $('<div></div>').append(newForm);
    $newLinkDiv.before($newFormDiv);

    // add a delete link to the new form
    addPhotoFormDeleteLink($newFormDiv);
}

function addPhotoFormDeleteLink($tagFormDiv) {
    var $removeFormButton = $('<button type="button">Delete photo</button>');
    $tagFormDiv.append($removeFormButton);

    $removeFormButton.on('click', function(e) {
        // remove the div for the tag form
        $tagFormDiv.remove();
    });
}