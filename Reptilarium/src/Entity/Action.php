<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 */
class Action
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Proie", mappedBy="Nourrissage")
     */
    private $Proie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="SerpentAction")
     */
    private $serpents;

    public function __construct()
    {
        $this->Proie = new ArrayCollection();
        $this->serpents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(?string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|Proie[]
     */
    public function getProie(): Collection
    {
        return $this->Proie;
    }

    public function addProie(Proie $proie): self
    {
        if (!$this->Proie->contains($proie)) {
            $this->Proie[] = $proie;
            $proie->setNourrissage($this);
        }

        return $this;
    }

    public function removeProie(Proie $proie): self
    {
        if ($this->Proie->contains($proie)) {
            $this->Proie->removeElement($proie);
            // set the owning side to null (unless already changed)
            if ($proie->getNourrissage() === $this) {
                $proie->setNourrissage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getSerpents(): Collection
    {
        return $this->serpents;
    }

    public function addSerpent(Serpent $serpent): self
    {
        if (!$this->serpents->contains($serpent)) {
            $this->serpents[] = $serpent;
            $serpent->setSerpentAction($this);
        }

        return $this;
    }

    public function removeSerpent(Serpent $serpent): self
    {
        if ($this->serpents->contains($serpent)) {
            $this->serpents->removeElement($serpent);
            // set the owning side to null (unless already changed)
            if ($serpent->getSerpentAction() === $this) {
                $serpent->setSerpentAction(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
     return $this->getType();

    }
}
