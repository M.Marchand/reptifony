<?php

namespace App\Repository;

use App\Entity\Proie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Proie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Proie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Proie[]    findAll()
 * @method Proie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Proie::class);
    }

    // /**
    //  * @return Proie[] Returns an array of Proie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Proie
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
