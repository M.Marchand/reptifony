<?php

namespace App\Form;

use App\Entity\Serpent;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Tests\Extension\Core\DataMapper\SubmittedForm;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tests\Fixtures\ActionArgumentsBundle\Controller\ActionArgumentsController;
use Vich\UploaderBundle\Entity\File;
use Vich\UploaderBundle\Form\Type\VichFileType;
use function Sodium\add;

class NewSnakeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Prenom')
            ->add('DateNaissance')
            ->add('Sexe')
            ->add('Espece')
            ->add('Origine')
            ->add('Description')
            ->add('Caractere')
            ->add('Serpent_habitat')
            ->add('Serpent_etat')
            ->add('Serpent_terrariophile')
            ->add('Serpent_action')
            ->add('Serpent_metries')
            ->add(
                'photos', CollectionType::class, array(
                    'entry_type' => EmbedPhotosType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'by_reference' => false,
                    'entry_options' => array('label' => false),
                )
            )
//            ->add('photoFile', VichFileType::class, [
//                'label' => ' (PDF file)'])

            ->add('save',SubmitType::class)

        ;



//pour afficher tous les autres qui représentent des entity commme serpenthabitat par exemple , il faudra rajouter dans l'entity en question une function__toString (voir la doc)
        ;
    }

    /**
     * @param OptionsResolver $resolver

     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Serpent::class,


        ]);


    }

}
