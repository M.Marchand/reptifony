<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Type;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="SerpentEtat")
     */
    private $serpents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo", mappedBy="PhotoEtat")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Alertes", inversedBy="alerteEtat")
     */
    private $alertes;

    public function __construct()
    {
        $this->serpents = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(?string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getSerpents(): Collection
    {
        return $this->serpents;
    }

    public function addSerpent(Serpent $serpent): self
    {
        if (!$this->serpents->contains($serpent)) {
            $this->serpents[] = $serpent;
            $serpent->setSerpentEtat($this);
        }

        return $this;
    }

    public function removeSerpent(Serpent $serpent): self
    {
        if ($this->serpents->contains($serpent)) {
            $this->serpents->removeElement($serpent);
            // set the owning side to null (unless already changed)
            if ($serpent->getSerpentEtat() === $this) {
                $serpent->setSerpentEtat(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->addPhotoEtat($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            $photo->removePhotoEtat($this);
        }

        return $this;
    }

    public function getAlertes(): ?Alertes
    {
        return $this->alertes;
    }

    public function setAlertes(?Alertes $alertes): self
    {
        $this->alertes = $alertes;

        return $this;
    }
}
