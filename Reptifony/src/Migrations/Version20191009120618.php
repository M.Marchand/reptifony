<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191009120618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE etat (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, date DATETIME DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE metries (id INT AUTO_INCREMENT NOT NULL, poids INT DEFAULT NULL, taille INT DEFAULT NULL, date DATETIME DEFAULT NULL, température INT DEFAULT NULL, hygrométrie INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE terrariophile (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) DEFAULT NULL, prénom VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, telephone VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE serpent (id INT AUTO_INCREMENT NOT NULL, serpent_habitat_id INT DEFAULT NULL, serpent_etat_id INT DEFAULT NULL, serpent_terrariophile_id INT DEFAULT NULL, serpent_action_id INT DEFAULT NULL, serpent_metries_id INT DEFAULT NULL, prénom VARCHAR(255) DEFAULT NULL, date_naissance DATETIME DEFAULT NULL, sexe VARCHAR(255) DEFAULT NULL, espèce VARCHAR(255) DEFAULT NULL, origine VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, caractère VARCHAR(255) DEFAULT NULL, INDEX IDX_12F262D5E13190A0 (serpent_habitat_id), INDEX IDX_12F262D569B83EC6 (serpent_etat_id), INDEX IDX_12F262D57A31723B (serpent_terrariophile_id), INDEX IDX_12F262D5560C22D7 (serpent_action_id), INDEX IDX_12F262D552E6754C (serpent_metries_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, date DATETIME DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proie (id INT AUTO_INCREMENT NOT NULL, nourrissage_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_34AAB6E125B8E3FD (nourrissage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo (id INT AUTO_INCREMENT NOT NULL, photo LONGTEXT DEFAULT NULL, date DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_serpent (photo_id INT NOT NULL, serpent_id INT NOT NULL, INDEX IDX_271275587E9E4C8C (photo_id), INDEX IDX_27127558E97BA8EC (serpent_id), PRIMARY KEY(photo_id, serpent_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_habitat (photo_id INT NOT NULL, habitat_id INT NOT NULL, INDEX IDX_ED7A5657E9E4C8C (photo_id), INDEX IDX_ED7A565AFFE2D26 (habitat_id), PRIMARY KEY(photo_id, habitat_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_etat (photo_id INT NOT NULL, etat_id INT NOT NULL, INDEX IDX_D4FB45967E9E4C8C (photo_id), INDEX IDX_D4FB4596D5E86FF (etat_id), PRIMARY KEY(photo_id, etat_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE photo_terrariophile (photo_id INT NOT NULL, terrariophile_id INT NOT NULL, INDEX IDX_E33C168B7E9E4C8C (photo_id), INDEX IDX_E33C168BEA517C1 (terrariophile_id), PRIMARY KEY(photo_id, terrariophile_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5E13190A0 FOREIGN KEY (serpent_habitat_id) REFERENCES habitat (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D569B83EC6 FOREIGN KEY (serpent_etat_id) REFERENCES etat (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D57A31723B FOREIGN KEY (serpent_terrariophile_id) REFERENCES terrariophile (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5560C22D7 FOREIGN KEY (serpent_action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D552E6754C FOREIGN KEY (serpent_metries_id) REFERENCES metries (id)');
        $this->addSql('ALTER TABLE proie ADD CONSTRAINT FK_34AAB6E125B8E3FD FOREIGN KEY (nourrissage_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE photo_serpent ADD CONSTRAINT FK_271275587E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_serpent ADD CONSTRAINT FK_27127558E97BA8EC FOREIGN KEY (serpent_id) REFERENCES serpent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_habitat ADD CONSTRAINT FK_ED7A5657E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_habitat ADD CONSTRAINT FK_ED7A565AFFE2D26 FOREIGN KEY (habitat_id) REFERENCES habitat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_etat ADD CONSTRAINT FK_D4FB45967E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_etat ADD CONSTRAINT FK_D4FB4596D5E86FF FOREIGN KEY (etat_id) REFERENCES etat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_terrariophile ADD CONSTRAINT FK_E33C168B7E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_terrariophile ADD CONSTRAINT FK_E33C168BEA517C1 FOREIGN KEY (terrariophile_id) REFERENCES terrariophile (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D569B83EC6');
        $this->addSql('ALTER TABLE photo_etat DROP FOREIGN KEY FK_D4FB4596D5E86FF');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D552E6754C');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D57A31723B');
        $this->addSql('ALTER TABLE photo_terrariophile DROP FOREIGN KEY FK_E33C168BEA517C1');
        $this->addSql('ALTER TABLE photo_serpent DROP FOREIGN KEY FK_27127558E97BA8EC');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D5560C22D7');
        $this->addSql('ALTER TABLE proie DROP FOREIGN KEY FK_34AAB6E125B8E3FD');
        $this->addSql('ALTER TABLE photo_serpent DROP FOREIGN KEY FK_271275587E9E4C8C');
        $this->addSql('ALTER TABLE photo_habitat DROP FOREIGN KEY FK_ED7A5657E9E4C8C');
        $this->addSql('ALTER TABLE photo_etat DROP FOREIGN KEY FK_D4FB45967E9E4C8C');
        $this->addSql('ALTER TABLE photo_terrariophile DROP FOREIGN KEY FK_E33C168B7E9E4C8C');
        $this->addSql('DROP TABLE etat');
        $this->addSql('DROP TABLE metries');
        $this->addSql('DROP TABLE terrariophile');
        $this->addSql('DROP TABLE serpent');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE proie');
        $this->addSql('DROP TABLE photo');
        $this->addSql('DROP TABLE photo_serpent');
        $this->addSql('DROP TABLE photo_habitat');
        $this->addSql('DROP TABLE photo_etat');
        $this->addSql('DROP TABLE photo_terrariophile');
    }
}
