<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191010131647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE alertes (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE etat ADD alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etat ADD CONSTRAINT FK_55CAF762A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('CREATE INDEX IDX_55CAF762A5EFBEEF ON etat (alertes_id)');
        $this->addSql('ALTER TABLE terrariophile ADD alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE terrariophile ADD CONSTRAINT FK_670D3C5CA5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('CREATE INDEX IDX_670D3C5CA5EFBEEF ON terrariophile (alertes_id)');
        $this->addSql('ALTER TABLE serpent ADD alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('CREATE INDEX IDX_12F262D5A5EFBEEF ON serpent (alertes_id)');
        $this->addSql('ALTER TABLE habitat ADD alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE habitat ADD CONSTRAINT FK_3B37B2E8A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('CREATE INDEX IDX_3B37B2E8A5EFBEEF ON habitat (alertes_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE etat DROP FOREIGN KEY FK_55CAF762A5EFBEEF');
        $this->addSql('ALTER TABLE terrariophile DROP FOREIGN KEY FK_670D3C5CA5EFBEEF');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D5A5EFBEEF');
        $this->addSql('ALTER TABLE habitat DROP FOREIGN KEY FK_3B37B2E8A5EFBEEF');
        $this->addSql('DROP TABLE alertes');
        $this->addSql('DROP INDEX IDX_55CAF762A5EFBEEF ON etat');
        $this->addSql('ALTER TABLE etat DROP alertes_id');
        $this->addSql('DROP INDEX IDX_3B37B2E8A5EFBEEF ON habitat');
        $this->addSql('ALTER TABLE habitat DROP alertes_id');
        $this->addSql('DROP INDEX IDX_12F262D5A5EFBEEF ON serpent');
        $this->addSql('ALTER TABLE serpent DROP alertes_id');
        $this->addSql('DROP INDEX IDX_670D3C5CA5EFBEEF ON terrariophile');
        $this->addSql('ALTER TABLE terrariophile DROP alertes_id');
    }
}
