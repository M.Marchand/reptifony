<?php

namespace App\Repository;

use App\Entity\Serpent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Serpent|null find($id, $lockMode = null, $lockVersion = null)
 * @method Serpent|null findOneBy(array $criteria, array $orderBy = null)
 * @method Serpent[]    findAll()
 * @method Serpent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SerpentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Serpent::class);
    }
public function searchSerpentByName($searchSerpent){
    $qb = $this->createQueryBuilder('s')
      ->where ('s.Prenom LIKE :searchSerpent')
      ->setParameter('searchSerpent', $searchSerpent);
    return $qb->getQuery()->getResult();


//      $this->render(view :'page_accueil/pageserpents.html.twig');

}

    // /**
    //  * @return Serpent[] Returns an array of Serpent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Serpent
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
