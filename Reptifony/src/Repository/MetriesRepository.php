<?php

namespace App\Repository;

use App\Entity\Metries;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Metries|null find($id, $lockMode = null, $lockVersion = null)
 * @method Metries|null findOneBy(array $criteria, array $orderBy = null)
 * @method Metries[]    findAll()
 * @method Metries[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Metries::class);
    }

    // /**
    //  * @return Metries[] Returns an array of Metries objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Metries
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
