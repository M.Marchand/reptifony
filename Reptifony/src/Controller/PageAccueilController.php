<?php

namespace App\Controller;

use App\Entity\Etat;
use App\Entity\Serpent;
use App\Form\FormualaireType;

use App\Repository\AlertesRepository;
use App\Repository\EtatRepository;
use App\Repository\HabitatRepository;
use App\Repository\MetriesRepository;
use App\Repository\PhotoRepository;
use App\Repository\SerpentRepository;
use App\Repository\TerrariophileRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageAccueilController extends AbstractController
{
    private $habitatRepository;
    private $serpentRepository;
    private $terrariophileRepository;
    private $etatRepository;
    private $metriesRepository;
    private $photoRepository;
    private $alerteRepository;


    public function __construct(EntityManagerInterface $entityManager ,HabitatRepository $habitatRepository ,SerpentRepository $serpentRepository , TerrariophileRepository $terrariophileRepository, EtatRepository $etatRepository, MetriesRepository $metriesRepository, PhotoRepository $photoRepository,AlertesRepository $alerteRepository)

    {
        $this->habitatRepository=$habitatRepository;
        $this->serpentRepository=$serpentRepository;
        $this->etatRepository=$etatRepository;
        $this->terrariophileRepository=$terrariophileRepository;
        $this->metriesRepository=$metriesRepository;
        $this->entityManager=$entityManager;
        $this->photoRepository=$photoRepository;
        $this->alerteRepository=$alerteRepository;
    }
    /**
     * @Route("/page/accueil", name="page_accueil")
     */
    public function index()
    {

        return $this->render('page_accueil/index.html.twig', [
            'controller_name' => 'PageAccueilController',
        ]);
    }

    /**
     * @Route("/pageserpents", name="page_serpents")
     */
    public function Serpent()
    {

        $array=$this->serpentRepository->searchSerpentByName( 'Jones');
        foreach ($array as $serpent)

            $prenom=$serpent->getPrenom();
            $birth=$serpent->getDateNaissance();
            $sexe=$serpent->getSexe();
            $espece=$serpent->getEspece();
            $origine=$serpent->getOrigine();
            $description=$serpent->getDescription();
            $caractere=$serpent->getCaractere();

            $newarray=$this->photoRepository->findAll();
        foreach ($newarray as $serpent)
            $photo=$serpent->getPhoto();
            ;






        return $this->render('page_accueil/pageserpents.html.twig', [
            'controller_name' => 'PageAccueilController',
            'prenom'=>$prenom,
            'birth'=>$birth,
            'sexe'=>$sexe,
            'espece'=>$espece,
            'origine'=>$origine,
            'description'=>$description,
            'caractere'=>$caractere,
             'photo'=>$photo,

        ]);

    }

    /**
     * @Route("/habitat", name="habitat")
     */
    public function Habitat()
    {
        $array=$this->habitatRepository->findAll();
        foreach ($array as $habitat){
            $hauteur=$habitat->getHauteur();
            $longueur=$habitat->getLongueur();
            $profondeur=$habitat->getProfondeur();
            $substrat=$habitat->getSubstrat();
            $systThermique=$habitat->getSystemeThermique();
            $systHygro=$habitat->getSystemeHygrometrique();}
        return $this->render('page_accueil/habitat.html.twig', [
            'controller_name' => 'PageAccueilController',
            'hauteur'=>$hauteur,
            'longueur'=>$longueur,
            'profondeur'=>$profondeur,
            'substrat'=>$substrat,
            'systThermique'=>$systThermique,
            'systHygro'=>$systHygro,

        ]);
    }

    /**
     * @Route("/santé", name="santé")
     */
    public function Etat()
    {
        $array=$this->etatRepository->findAll();
        foreach ($array as $etat){
            $type=$etat->getType();
            $date=$etat->getDate();
            $description=$etat->getDescription();
        }
        $array=$this->metriesRepository->findAll();
        foreach ($array as $metries){
            $poids=$metries->getPoids();
            $taille=$metries->getTaille();
            $datemetries=$metries->getDate();
            $temperature=$metries->getTemperature();
            $hygrometrie=$metries->getHygrometrie();
        }
        return $this->render('page_accueil/santé.html.twig', [
            'controller_name' => 'PageAccueilController',
            'type'=>$type,
             'date'=>$date,
            'description'=>$description,
            'poids'=>$poids,
            'taille'=>$taille,
            'datemetries'=>$datemetries,
            'temperature'=>$temperature,
            'hygrometrie'=>$hygrometrie,

        ]);
    }


    /**
     * @Route("/terrariophile", name="terrariophile")
     */
    public function Terrariophile()
    {
        $this->getUser();
        $array=$this->terrariophileRepository->findAll();
        foreach ($array as $terrariophile){
            $nom=$terrariophile->getNom();
            $prenom=$terrariophile->getPrenom();
            $mail=$terrariophile->getMail();
            $tel=$terrariophile->getTelephone();

        }
        return $this->render('page_accueil/terrariophile.html.twig', [
            'controller_name' => 'PageAccueilController',
            'nom'=>$nom,
            'prenom'=>$prenom,
            'mail'=>$mail,
            'telephone'=>$tel,

        ]);
    }

    /**
     * @Route("/alertes", name="alertes")
     */
    public function Alertes()
    {


//        $form = new FormualaireType();
//            $this->createFormBuilder($form);
//
//        $form = $this->createForm(Serpent::class, $form);





        return $this->render('page_accueil/alertes.html.twig', [
            'controller_name' => 'PageAccueilController',
//            'form' => $form,
        ]);
    }

}


