<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031101529 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE etat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE metries CHANGE poids poids INT DEFAULT NULL, CHANGE taille taille INT DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE temperature temperature INT DEFAULT NULL, CHANGE hygrometrie hygrometrie INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
        $this->addSql('ALTER TABLE terrariophile CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(255) DEFAULT NULL, CHANGE password password VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE alertes CHANGE type type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE serpent CHANGE serpent_habitat_id serpent_habitat_id INT DEFAULT NULL, CHANGE serpent_etat_id serpent_etat_id INT DEFAULT NULL, CHANGE serpent_terrariophile_id serpent_terrariophile_id INT DEFAULT NULL, CHANGE serpent_action_id serpent_action_id INT DEFAULT NULL, CHANGE serpent_metries_id serpent_metries_id INT DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE date_naissance date_naissance DATETIME DEFAULT NULL, CHANGE sexe sexe VARCHAR(255) DEFAULT NULL, CHANGE origine origine VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE espece espece VARCHAR(255) DEFAULT NULL, CHANGE caractere caractere VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE habitat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE hauteur hauteur INT DEFAULT NULL, CHANGE longueur longueur INT DEFAULT NULL, CHANGE profondeur profondeur INT DEFAULT NULL, CHANGE substrat substrat VARCHAR(255) DEFAULT NULL, CHANGE systeme_thermique systeme_thermique VARCHAR(255) DEFAULT NULL, CHANGE systeme_hygrometrique systeme_hygrometrique VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE action CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE proie CHANGE nourrissage_id nourrissage_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE photo CHANGE date date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE action CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE alertes CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE etat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE habitat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE hauteur hauteur INT DEFAULT NULL, CHANGE longueur longueur INT DEFAULT NULL, CHANGE profondeur profondeur INT DEFAULT NULL, CHANGE substrat substrat VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE systeme_thermique systeme_thermique VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE systeme_hygrometrique systeme_hygrometrique VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE metries CHANGE poids poids INT DEFAULT NULL, CHANGE taille taille INT DEFAULT NULL, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE temperature temperature INT DEFAULT NULL, CHANGE hygrometrie hygrometrie INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photo CHANGE date date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE proie CHANGE nourrissage_id nourrissage_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE serpent CHANGE serpent_habitat_id serpent_habitat_id INT DEFAULT NULL, CHANGE serpent_etat_id serpent_etat_id INT DEFAULT NULL, CHANGE serpent_terrariophile_id serpent_terrariophile_id INT DEFAULT NULL, CHANGE serpent_action_id serpent_action_id INT DEFAULT NULL, CHANGE serpent_metries_id serpent_metries_id INT DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date_naissance date_naissance DATETIME DEFAULT \'NULL\', CHANGE sexe sexe VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE espece espece VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE origine origine VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE caractere caractere VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE terrariophile CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE telephone telephone VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE password password VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
