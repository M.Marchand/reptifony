<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HabitatRepository")
 */
class Habitat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Hauteur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Longueur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $Profondeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Substrat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SystemeThermique;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $SystemeHygrometrique;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="SerpentHabitat")
     */
    private $serpents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo", mappedBy="PhotoHabitat")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Alertes", inversedBy="alerteHabitat")
     */
    private $alertes;

    public function __construct()
    {
        $this->serpents = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHauteur(): ?int
    {
        return $this->Hauteur;
    }

    public function setHauteur(?int $Hauteur): self
    {
        $this->Hauteur = $Hauteur;

        return $this;
    }

    public function getLongueur(): ?int
    {
        return $this->Longueur;
    }

    public function setLongueur(?int $Longueur): self
    {
        $this->Longueur = $Longueur;

        return $this;
    }

    public function getProfondeur(): ?int
    {
        return $this->Profondeur;
    }

    public function setProfondeur(?int $Profondeur): self
    {
        $this->Profondeur = $Profondeur;

        return $this;
    }

    public function getSubstrat(): ?string
    {
        return $this->Substrat;
    }

    public function setSubstrat(?string $Substrat): self
    {
        $this->Substrat = $Substrat;

        return $this;
    }

    public function getSystemeThermique(): ?string
    {
        return $this->SystemeThermique;
    }

    public function setSystemeThermique(?string $SystemeThermique): self
    {
        $this->SystemeThermique = $SystemeThermique;

        return $this;
    }

    public function getSystemeHygrometrique(): ?string
    {
        return $this->SystemeHygrometrique ;
    }

    public function setSystemeHygrometrique(?string $SystemeHygrometrique): self
    {
        $this->SystemeHygrometrique = $SystemeHygrometrique;

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getSerpents(): Collection
    {
        return $this->serpents;
    }

    public function addSerpent(Serpent $serpent): self
    {
        if (!$this->serpents->contains($serpent)) {
            $this->serpents[] = $serpent;
            $serpent->setSerpentHabitat($this);
        }

        return $this;
    }

    public function removeSerpent(Serpent $serpent): self
    {
        if ($this->serpents->contains($serpent)) {
            $this->serpents->removeElement($serpent);
            // set the owning side to null (unless already changed)
            if ($serpent->getSerpentHabitat() === $this) {
                $serpent->setSerpentHabitat(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->addPhotoHabitat($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            $photo->removePhotoHabitat($this);
        }

        return $this;
    }

    public function getAlertes(): ?Alertes
    {
        return $this->alertes;
    }

    public function setAlertes(?Alertes $alertes): self
    {
        $this->alertes = $alertes;

        return $this;
    }
}
