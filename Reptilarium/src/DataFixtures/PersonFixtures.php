<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Terrariophile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PersonFixtures extends Fixture
{
    public const PERSON_REFERENCE = 'person_';

    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 20; $i++) { 
            $person = new Terrariophile();
            $person->setPrenom("Person $i");
            $person->setMail('martinet@yahoo.fr');
            $manager->persist($person);

            $this->addReference(self::PERSON_REFERENCE . $i, $person);
        }

        $manager->flush();
    }
}
