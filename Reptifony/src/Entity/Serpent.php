<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SerpentRepository")
 */
class Serpent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $DateNaissance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Sexe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Espece;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Origine;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Caractere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Habitat", inversedBy="serpents")
     */
    private $SerpentHabitat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etat", inversedBy="serpents")
     */
    private $SerpentEtat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Terrariophile", inversedBy="serpents")
     */
    private $SerpentTerrariophile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Action", inversedBy="serpents")
     */
    private $SerpentAction;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Metries", inversedBy="serpents")
     */
    private $SerpentMetries;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo", mappedBy="PhotoSerpent")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Alertes", inversedBy="alerteSerpent")
     */
    private $alertes;

    public function __construct()
    {
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(?string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->DateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $DateNaissance): self
    {
        $this->DateNaissance = $DateNaissance;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->Sexe;
    }

    public function setSexe(?string $Sexe): self
    {
        $this->Sexe = $Sexe;

        return $this;
    }

    public function getEspece(): ?string
    {
        return $this->Espece;
    }

    public function setEspece(?string $Espece): self
    {
        $this->Espèce = $Espece;

        return $this;
    }

    public function getOrigine(): ?string
    {
        return $this->Origine;
    }

    public function setOrigine(?string $Origine): self
    {
        $this->Origine = $Origine;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getCaractere(): ?string
    {
        return $this->Caractere;
    }

    public function setCaractere(?string $Caractere): self
    {
        $this->Caractere = $Caractere;

        return $this;
    }

    public function getSerpentHabitat(): ?Habitat
    {
        return $this->SerpentHabitat;
    }

    public function setSerpentHabitat(?Habitat $SerpentHabitat): self
    {
        $this->SerpentHabitat = $SerpentHabitat;

        return $this;
    }

    public function getSerpentEtat(): ?Etat
    {
        return $this->SerpentEtat;
    }

    public function setSerpentEtat(?Etat $SerpentEtat): self
    {
        $this->SerpentEtat = $SerpentEtat;

        return $this;
    }

    public function getSerpentTerrariophile(): ?Terrariophile
    {
        return $this->SerpentTerrariophile;
    }

    public function setSerpentTerrariophile(?Terrariophile $SerpentTerrariophile): self
    {
        $this->SerpentTerrariophile = $SerpentTerrariophile;

        return $this;
    }

    public function getSerpentAction(): ?Action
    {
        return $this->SerpentAction;
    }

    public function setSerpentAction(?Action $SerpentAction): self
    {
        $this->SerpentAction = $SerpentAction;

        return $this;
    }

    public function getSerpentMetries(): ?Metries
    {
        return $this->SerpentMetries;
    }

    public function setSerpentMetries(?Metries $SerpentMetries): self
    {
        $this->SerpentMetries = $SerpentMetries;

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->addPhotoSerpent($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            $photo->removePhotoSerpent($this);
        }

        return $this;
    }

    public function getAlertes(): ?Alertes
    {
        return $this->alertes;
    }

    public function setAlertes(?Alertes $alertes): self
    {
        $this->alertes = $alertes;

        return $this;
    }
}
