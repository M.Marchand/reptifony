<?php

namespace App\DataFixtures;


use App\Entity\Terrariophile;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $person = new Terrariophile();
         $person->setPrenom('JuZED');

         $manager->persist($person);

        $manager->flush();
    }
}
