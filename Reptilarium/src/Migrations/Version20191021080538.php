<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191021080538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE etat CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE etat ADD CONSTRAINT FK_55CAF762A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('ALTER TABLE metries CHANGE poids poids INT DEFAULT NULL, CHANGE taille taille INT DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE temperature temperature INT DEFAULT NULL, CHANGE hygrometrie hygrometrie INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD password VARCHAR(255) NOT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE roles roles JSON NOT NULL, ADD PRIMARY KEY (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495126AC48 ON user (mail)');
        $this->addSql('ALTER TABLE terrariophile DROP password, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE nom nom VARCHAR(255) DEFAULT NULL, CHANGE telephone telephone VARCHAR(255) DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE terrariophile ADD CONSTRAINT FK_670D3C5CA5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('ALTER TABLE alertes CHANGE type type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE serpent CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE serpent_habitat_id serpent_habitat_id INT DEFAULT NULL, CHANGE serpent_etat_id serpent_etat_id INT DEFAULT NULL, CHANGE serpent_terrariophile_id serpent_terrariophile_id INT DEFAULT NULL, CHANGE serpent_action_id serpent_action_id INT DEFAULT NULL, CHANGE serpent_metries_id serpent_metries_id INT DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT NULL, CHANGE date_naissance date_naissance DATETIME DEFAULT NULL, CHANGE sexe sexe VARCHAR(255) DEFAULT NULL, CHANGE origine origine VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL, CHANGE espece espece VARCHAR(255) DEFAULT NULL, CHANGE caractere caractere VARCHAR(255) DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5E13190A0 FOREIGN KEY (serpent_habitat_id) REFERENCES habitat (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D569B83EC6 FOREIGN KEY (serpent_etat_id) REFERENCES etat (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D57A31723B FOREIGN KEY (serpent_terrariophile_id) REFERENCES terrariophile (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5560C22D7 FOREIGN KEY (serpent_action_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D552E6754C FOREIGN KEY (serpent_metries_id) REFERENCES metries (id)');
        $this->addSql('ALTER TABLE serpent ADD CONSTRAINT FK_12F262D5A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('ALTER TABLE habitat CHANGE hauteur hauteur INT DEFAULT NULL, CHANGE longueur longueur INT DEFAULT NULL, CHANGE profondeur profondeur INT DEFAULT NULL, CHANGE substrat substrat VARCHAR(255) DEFAULT NULL, CHANGE systeme_thermique systeme_thermique VARCHAR(255) DEFAULT NULL, CHANGE systeme_hygrometrique systeme_hygrometrique VARCHAR(255) DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE habitat ADD CONSTRAINT FK_3B37B2E8A5EFBEEF FOREIGN KEY (alertes_id) REFERENCES alertes (id)');
        $this->addSql('ALTER TABLE action CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE date date DATETIME DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE proie CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE nourrissage_id nourrissage_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL, CHANGE description description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE proie ADD CONSTRAINT FK_34AAB6E125B8E3FD FOREIGN KEY (nourrissage_id) REFERENCES action (id)');
        $this->addSql('ALTER TABLE photo CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE date date DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE photo_serpent ADD CONSTRAINT FK_271275587E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_serpent ADD CONSTRAINT FK_27127558E97BA8EC FOREIGN KEY (serpent_id) REFERENCES serpent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_habitat ADD CONSTRAINT FK_ED7A5657E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_habitat ADD CONSTRAINT FK_ED7A565AFFE2D26 FOREIGN KEY (habitat_id) REFERENCES habitat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_etat ADD CONSTRAINT FK_D4FB45967E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_etat ADD CONSTRAINT FK_D4FB4596D5E86FF FOREIGN KEY (etat_id) REFERENCES etat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_terrariophile ADD CONSTRAINT FK_E33C168B7E9E4C8C FOREIGN KEY (photo_id) REFERENCES photo (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE photo_terrariophile ADD CONSTRAINT FK_E33C168BEA517C1 FOREIGN KEY (terrariophile_id) REFERENCES terrariophile (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE action CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE alertes CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE etat DROP FOREIGN KEY FK_55CAF762A5EFBEEF');
        $this->addSql('ALTER TABLE etat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE habitat DROP FOREIGN KEY FK_3B37B2E8A5EFBEEF');
        $this->addSql('ALTER TABLE habitat CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE hauteur hauteur INT DEFAULT NULL, CHANGE longueur longueur INT DEFAULT NULL, CHANGE profondeur profondeur INT DEFAULT NULL, CHANGE substrat substrat VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE systeme_thermique systeme_thermique VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE systeme_hygrometrique systeme_hygrometrique VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE metries CHANGE poids poids INT DEFAULT NULL, CHANGE taille taille INT DEFAULT NULL, CHANGE date date DATETIME DEFAULT \'NULL\', CHANGE temperature temperature INT DEFAULT NULL, CHANGE hygrometrie hygrometrie INT DEFAULT NULL');
        $this->addSql('ALTER TABLE photo CHANGE id id INT NOT NULL, CHANGE date date DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE photo_etat DROP FOREIGN KEY FK_D4FB45967E9E4C8C');
        $this->addSql('ALTER TABLE photo_etat DROP FOREIGN KEY FK_D4FB4596D5E86FF');
        $this->addSql('ALTER TABLE photo_habitat DROP FOREIGN KEY FK_ED7A5657E9E4C8C');
        $this->addSql('ALTER TABLE photo_habitat DROP FOREIGN KEY FK_ED7A565AFFE2D26');
        $this->addSql('ALTER TABLE photo_serpent DROP FOREIGN KEY FK_271275587E9E4C8C');
        $this->addSql('ALTER TABLE photo_serpent DROP FOREIGN KEY FK_27127558E97BA8EC');
        $this->addSql('ALTER TABLE photo_terrariophile DROP FOREIGN KEY FK_E33C168B7E9E4C8C');
        $this->addSql('ALTER TABLE photo_terrariophile DROP FOREIGN KEY FK_E33C168BEA517C1');
        $this->addSql('ALTER TABLE proie DROP FOREIGN KEY FK_34AAB6E125B8E3FD');
        $this->addSql('ALTER TABLE proie CHANGE id id INT NOT NULL, CHANGE nourrissage_id nourrissage_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D5E13190A0');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D569B83EC6');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D57A31723B');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D5560C22D7');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D552E6754C');
        $this->addSql('ALTER TABLE serpent DROP FOREIGN KEY FK_12F262D5A5EFBEEF');
        $this->addSql('ALTER TABLE serpent CHANGE id id INT NOT NULL, CHANGE serpent_habitat_id serpent_habitat_id INT DEFAULT NULL, CHANGE serpent_etat_id serpent_etat_id INT DEFAULT NULL, CHANGE serpent_terrariophile_id serpent_terrariophile_id INT DEFAULT NULL, CHANGE serpent_action_id serpent_action_id INT DEFAULT NULL, CHANGE serpent_metries_id serpent_metries_id INT DEFAULT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE prenom prenom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE date_naissance date_naissance DATETIME DEFAULT \'NULL\', CHANGE sexe sexe VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE espece espece VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE origine origine VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE description description VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE caractere caractere VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE terrariophile DROP FOREIGN KEY FK_670D3C5CA5EFBEEF');
        $this->addSql('ALTER TABLE terrariophile ADD password VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE id id INT NOT NULL, CHANGE alertes_id alertes_id INT DEFAULT NULL, CHANGE nom nom VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci, CHANGE telephone telephone VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user MODIFY id INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D6495126AC48 ON user');
        $this->addSql('ALTER TABLE user DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE user DROP password, CHANGE id id INT NOT NULL, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
