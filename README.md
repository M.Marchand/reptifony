Reptilarium est une application qui permet d'effectuer un suivi de reptiles , dans le cas d'un maintien en captivité. Ce projet a entièrement été réalisé en Symfony.

Fonctionnalités mise en place :
Login/register
Back-Office easy-admin
Formulaires d'ajouts/modifications
Upload de photos avec VichUploader