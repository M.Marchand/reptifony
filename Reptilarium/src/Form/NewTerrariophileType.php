<?php

namespace App\Form;

use App\Entity\Terrariophile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewTerrariophileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Nom')
            ->add('Prenom')
            ->add('Mail')
            ->add('Telephone')
            ->add(
                'photos', CollectionType::class, array(
                    'entry_type' => EmbedPhotosTerrariophileType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    'by_reference' => false,
                    'entry_options' => array('label' => false),
                )
            )
            ->add('save',SubmitType::class)   ;
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Terrariophile::class,
        ]);
    }
}
