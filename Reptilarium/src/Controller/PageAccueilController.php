<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Alertes;
use App\Entity\Etat;
use App\Entity\Habitat;
use App\Entity\Metries;
use App\Entity\Photo;
use App\Entity\Proie;
use App\Entity\Serpent;


use App\Entity\Terrariophile;
use App\Form\NewActionType;
use App\Form\NewAlerteType;
use App\Form\NewEtatType;
use App\Form\NewHabitatType;
use App\Form\NewMetriesType;
use App\Form\NewProieType;
use App\Form\NewSnakeType;
use App\Form\NewTerrariophileType;
use App\Form\PhotosType;
use App\Repository\ActionRepository;
use App\Repository\AlertesRepository;
use App\Repository\EtatRepository;
use App\Repository\HabitatRepository;
use App\Repository\MetriesRepository;
use App\Repository\PhotoRepository;
use App\Repository\ProieRepository;
use App\Repository\SerpentRepository;
use App\Repository\TerrariophileRepository;
use Doctrine\ORM\EntityManagerInterface;

use http\Env\Response;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function Sodium\add;


class PageAccueilController extends AbstractController
{
    private $habitatRepository;
    private $serpentRepository;
    private $terrariophileRepository;
    private $etatRepository;
    private $metriesRepository;
    private $photoRepository;
    private $alerteRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    private $proieRepository;
    private $actionRepository;


    public function __construct( ActionRepository $actionRepository, EntityManagerInterface $entityManager ,HabitatRepository $habitatRepository ,SerpentRepository $serpentRepository , TerrariophileRepository $terrariophileRepository, EtatRepository $etatRepository, MetriesRepository $metriesRepository, PhotoRepository $photoRepository,AlertesRepository $alerteRepository, \Swift_Mailer $mailer, ProieRepository $proieRepository)

    {
        $this->habitatRepository=$habitatRepository;
        $this->serpentRepository=$serpentRepository;
        $this->etatRepository=$etatRepository;
        $this->terrariophileRepository=$terrariophileRepository;
        $this->metriesRepository=$metriesRepository;
        $this->entityManager=$entityManager;
        $this->photoRepository=$photoRepository;
        $this->alerteRepository=$alerteRepository;
        $this->mailer=$mailer;
        $this->proieRepository=$proieRepository;
        $this->actionRepository=$actionRepository;



    }
    /**
     * @Route("/page/accueil", name="page_accueil")
     */
    public function index()
    {

        return $this->render('page_accueil/index.html.twig', [
            'controller_name' => 'PageAccueilController',
        ]);
    }

    /**
     * @Route("/pageserpents", name="page_serpents")
     *
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function Serpent(Request $request)
    {

        $array=$this->terrariophileRepository->findAll();
        $terrariophile= $this->getUser();
        $serpentTerrariophile= $terrariophile->getSerpents()->toArray();




        $nom=$terrariophile->getNom();
        $prenom=$terrariophile->getPrenom();
        $mail=$terrariophile->getMail();
        $tel=$terrariophile->getTelephone();


        $newSnake=new Serpent();
        $formSnake=$this->createForm(NewSnakeType::class, $newSnake);
        $formSnake->handleRequest($request);
        if($formSnake->isSubmitted() && $formSnake->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newSnake);
            $entityManager->flush();
            return $this->redirectToRoute('page_accueil');
        }
//        $newarray=$this->photoRepository->findAll();
//        foreach ($newarray as $serpent)
//            $photo=$serpent->getPhotoSerpent();
//        ;







        $array=$this->serpentRepository->findAll();
//foreach ($array as $serpent)
//    $action=$serpent->getSerpentAction();
//     $habitat=$serpent->getSerpentHabitat();


        return $this->render('page_accueil/pageserpents.html.twig', array(

          'serpents'=>$array,
//           'mesphotos'=>$newarray,
            'SerpentTerrariophile'=>$serpentTerrariophile,
           'formSnake'=>$formSnake->createView(),

//            'photo'=>$photo,
//
        ));

    }

    /**
     * @Route("/habitat", name="habitat")
     */
    public function Habitat(Request $request)
    {



        $array=$this->habitatRepository->findAll();



        $newhabitat = new Habitat();
        $formHabitat=$this->createForm(NewHabitatType::class,$newhabitat);
        $formHabitat->handleRequest($request);
        if ($formHabitat->isSubmitted()&& $formHabitat->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($newhabitat);
            $entityManager->flush();
        }

//        foreach ($array as $habitat){
//            $hauteur=$habitat->getHauteur();
//            $longueur=$habitat->getLongueur();
//            $profondeur=$habitat->getProfondeur();
//            $substrat=$habitat->getSubstrat();
//            $systThermique=$habitat->getSystemeThermique();
//            $systHygro=$habitat->getSystemeHygrometrique();}
        return $this->render('page_accueil/habitat.html.twig', [
//            'controller_name' => 'PageAccueilController',
//            'hauteur'=>$hauteur,
//            'longueur'=>$longueur,
//            'profondeur'=>$profondeur,
//            'substrat'=>$substrat,
//            'systThermique'=>$systThermique,
//            'systHygro'=>$systHygro,

            'habitat'=>$array,
            'formHabitat'=>$formHabitat->createView(),

        ]);
    }

    /**
     * @Route("/santé", name="santé")
     */
    public function Etat(Request $request)
    {
        $array=$this->etatRepository->findAll();
        foreach ($array as $etat){
            $type=$etat->getType();
            $date=$etat->getDate();
            $description=$etat->getDescription();
        }
        $newEtat=new Etat();
        $formEtat=$this->createForm(NewEtatType::class, $newEtat);
        $formEtat->handleRequest($request);
        if($formEtat->isSubmitted() && $formEtat->isValid()){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newEtat);
            $entityManager->flush();
            return $this->redirectToRoute('page_accueil');
        }
        $array=$this->metriesRepository->findAll();
        foreach ($array as $metries){
            $poids=$metries->getPoids();
            $taille=$metries->getTaille();
            $datemetries=$metries->getDate();
            $temperature=$metries->getTemperature();
            $hygrometrie=$metries->getHygrometrie();
        }
        $newMetries= new Metries();
        $formMetries=$this->createForm(NewMetriesType::class,$newMetries);
        $formMetries->handleRequest($request);
        if ($formMetries->isSubmitted()&& $formMetries->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($newMetries);
            $entityManager->flush();
        }
        return $this->render('page_accueil/santé.html.twig', array(
            'controller_name' => 'PageAccueilController',
            'type'=>$type,
            'date'=>$date,
            'description'=>$description,
            'poids'=>$poids,
            'taille'=>$taille,
            'datemetries'=>$datemetries,
            'temperature'=>$temperature,
            'hygrometrie'=>$hygrometrie,
            'formEtat'=>$formEtat->createView(),
            'formMetries'=>$formMetries->createView(),
        )


        );

    }


    /**
     * @Route("/terrariophile", name="terrariophile")
     */
    public function Terrariophile(Request $request)
    {

        $array=$this->terrariophileRepository->findAll();
        $terrariophile= $this->getUser();
        $serpentTerrariophile= $terrariophile->getSerpents()->toArray();




            $nom=$terrariophile->getNom();
            $prenom=$terrariophile->getPrenom();
            $mail=$terrariophile->getMail();
            $tel=$terrariophile->getTelephone();


        $newTerrariophile= new Terrariophile();
        $formTerrariophile=$this->createForm(NewTerrariophileType:: class,$newTerrariophile);
        $formTerrariophile->handleRequest($request);
        if ($formTerrariophile->isSubmitted()&& $formTerrariophile->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($newTerrariophile);
            $entityManager->flush();
        }

        return $this->render('page_accueil/terrariophile.html.twig', [
            'controller_name' => 'PageAccueilController',
            'nom'=>$nom,
            'prenom'=>$prenom,
            'mail'=>$mail,
            'telephone'=>$tel,
             'SerpentTerrariophile'=>$serpentTerrariophile,
            'formTerrariophile'=>$formTerrariophile->createView(),

        ]);
    }

    /**
     * @Route("/alertes", name="alertes")
     */
    public function Alertes(Request $request)
    {
        $newAlerte= new Alertes();
        $formAlerte=$this->createForm(NewAlerteType::class,$newAlerte);
        $formAlerte->handleRequest($request);
        if ($formAlerte->isSubmitted()&& $formAlerte->isValid()){
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($newAlerte);
            $entityManager->flush();
        }




        return $this->render('page_accueil/alertes.html.twig', [
            'controller_name' => 'PageAccueilController',
             'formAlerte'=>$formAlerte->createView(),

//
        ]);
    }
    /**
     * @Route("/action", name="action")
     */

    public function Action(Request $request){

    $newAction= new Action();
    $formAction=$this->createForm(NewActionType::class, $newAction);
    $formAction->handleRequest($request);
    if ($formAction->isSubmitted()&& $formAction->isValid()){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($newAction);
        $entityManager->flush();
    }
    $newProie = new Proie();
    $formProie=$this->createForm(NewProieType::class,$newProie);
    $formProie->handleRequest($request);
    if ($formProie->isSubmitted()&&$formProie->isValid()){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($newProie);
        $entityManager->flush();
    }
        $array=$this->proieRepository->findAll();
        $actionArray=$this->actionRepository->findAll();

        return $this->render('page_accueil/action.html.twig', [
        'formAction'=>$formAction->createView(),
        'formProie'=>$formProie->createView(),
            'proie'=>$array,
            'action'=>$actionArray,
        ]);

}
    /**
     * @Route("/photos", name="photos")
     */
    public function Photos(Request $request){
        $newPhoto= new Photo();
        $photoForm= $this->createForm(PhotosType::class,$newPhoto);
        $photoForm->handleRequest($request);
        if ($photoForm->isSubmitted()&&$photoForm->isValid()){
            $entityMananger=$this->getDoctrine()->getManager();
            $entityMananger->persist($newPhoto);
            $entityMananger->flush();
        }
        return $this->render('page_accueil/photos.html.twig', [
            'photoForm'=>$photoForm->createView(),

        ]);
    }





}


