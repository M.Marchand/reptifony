<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlertesRepository")
 */
class Alertes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Habitat", mappedBy="alertes")
     */
    private $alerteHabitat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="alertes")
     */
    private $alerteSerpent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etat", mappedBy="alertes")
     */
    private $alerteEtat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Terrariophile", mappedBy="alertes")
     */
    private $alerteTerrariophile;

    public function __construct()
    {
        $this->alerteHabitat = new ArrayCollection();
        $this->alerteSerpent = new ArrayCollection();
        $this->alerteEtat = new ArrayCollection();
        $this->alerteTerrariophile = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(?string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    /**
     * @return Collection|Habitat[]
     */
    public function getAlerteHabitat(): Collection
    {
        return $this->alerteHabitat;
    }

    public function addAlerteHabitat(Habitat $alerteHabitat): self
    {
        if (!$this->alerteHabitat->contains($alerteHabitat)) {
            $this->alerteHabitat[] = $alerteHabitat;
            $alerteHabitat->setAlertes($this);
        }

        return $this;
    }

    public function removeAlerteHabitat(Habitat $alerteHabitat): self
    {
        if ($this->alerteHabitat->contains($alerteHabitat)) {
            $this->alerteHabitat->removeElement($alerteHabitat);
            // set the owning side to null (unless already changed)
            if ($alerteHabitat->getAlertes() === $this) {
                $alerteHabitat->setAlertes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getAlerteSerpent(): Collection
    {
        return $this->alerteSerpent;
    }

    public function addAlerteSerpent(Serpent $alerteSerpent): self
    {
        if (!$this->alerteSerpent->contains($alerteSerpent)) {
            $this->alerteSerpent[] = $alerteSerpent;
            $alerteSerpent->setAlertes($this);
        }

        return $this;
    }

    public function removeAlerteSerpent(Serpent $alerteSerpent): self
    {
        if ($this->alerteSerpent->contains($alerteSerpent)) {
            $this->alerteSerpent->removeElement($alerteSerpent);
            // set the owning side to null (unless already changed)
            if ($alerteSerpent->getAlertes() === $this) {
                $alerteSerpent->setAlertes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Etat[]
     */
    public function getAlerteEtat(): Collection
    {
        return $this->alerteEtat;
    }

    public function addAlerteEtat(Etat $alerteEtat): self
    {
        if (!$this->alerteEtat->contains($alerteEtat)) {
            $this->alerteEtat[] = $alerteEtat;
            $alerteEtat->setAlertes($this);
        }

        return $this;
    }

    public function removeAlerteEtat(Etat $alerteEtat): self
    {
        if ($this->alerteEtat->contains($alerteEtat)) {
            $this->alerteEtat->removeElement($alerteEtat);
            // set the owning side to null (unless already changed)
            if ($alerteEtat->getAlertes() === $this) {
                $alerteEtat->setAlertes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Terrariophile[]
     */
    public function getAlerteTerrariophile(): Collection
    {
        return $this->alerteTerrariophile;
    }

    public function addAlerteTerrariophile(Terrariophile $alerteTerrariophile): self
    {
        if (!$this->alerteTerrariophile->contains($alerteTerrariophile)) {
            $this->alerteTerrariophile[] = $alerteTerrariophile;
            $alerteTerrariophile->setAlertes($this);
        }

        return $this;
    }

    public function removeAlerteTerrariophile(Terrariophile $alerteTerrariophile): self
    {
        if ($this->alerteTerrariophile->contains($alerteTerrariophile)) {
            $this->alerteTerrariophile->removeElement($alerteTerrariophile);
            // set the owning side to null (unless already changed)
            if ($alerteTerrariophile->getAlertes() === $this) {
                $alerteTerrariophile->setAlertes(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getType();
    }
}
