<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Mapping as ORM;

use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 *  @ORM\Table(name="photo")
 * @ORM\Entity(repositoryClass="App\Repository\PhotoRepository")
 * @Vich\Uploadable
 */
class Photo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Photo;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Serpent", inversedBy="photos")
     */
    private $PhotoSerpent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Habitat", inversedBy="photos")
     */
    private $PhotoHabitat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Etat", inversedBy="photos")
     */
    private $PhotoEtat;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Terrariophile", inversedBy="photos")
     */
    private $PhotoTerrariophile;
    /**
     * @Vich\UploadableField(mapping="file_photo", fileNameProperty="Photo")
     */
    private $photoFile;
    /**
     * @var \DateTimeImmutable
     */
    private $updateAt;



    /**
     * @var File|null
     */
    private $SerpentPhoto;

    public function __construct()
    {
        $this->PhotoSerpent = new ArrayCollection();
        $this->PhotoHabitat = new ArrayCollection();
        $this->PhotoEtat = new ArrayCollection();
        $this->PhotoTerrariophile = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(?\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }


    /**
     * @return Collection|Serpent[]
     */
    /**
     * @return mixed
     */
    public function getphotoFile():?File
    {
        return $this->photoFile;
    }
    /**
     * @param File|null $photoFile
     * @throws \Exception
     */
    public function setphotoFile(?File $photoFile=null):void{
        $this->photoFile=$photoFile;
        if ($photoFile){
            $this->updateAt= new\DateTimeImmutable('now');
        }
    }

    /**
     * @return Collection
     */
    public function getPhotoSerpent(): Collection
    {
        return $this->PhotoSerpent;
    }

    public function addPhotoSerpent(Serpent $photoSerpent): self
    {
        if (!$this->PhotoSerpent->contains($photoSerpent)) {
            $this->PhotoSerpent[] = $photoSerpent;
        }

        return $this;
    }

    public function removePhotoSerpent(Serpent $photoSerpent): self
    {
        if ($this->PhotoSerpent->contains($photoSerpent)) {
            $this->PhotoSerpent->removeElement($photoSerpent);
        }

        return $this;
    }

    /**
     * @return Collection|Habitat[]
     */
    public function getPhotoHabitat(): Collection
    {
        return $this->PhotoHabitat;
    }

    public function addPhotoHabitat(Habitat $photoHabitat): self
    {
        if (!$this->PhotoHabitat->contains($photoHabitat)) {
            $this->PhotoHabitat[] = $photoHabitat;
        }

        return $this;
    }

    public function removePhotoHabitat(Habitat $photoHabitat): self
    {
        if ($this->PhotoHabitat->contains($photoHabitat)) {
            $this->PhotoHabitat->removeElement($photoHabitat);
        }

        return $this;
    }

    /**
     * @return Collection|Etat[]
     */
    public function getPhotoEtat(): Collection
    {
        return $this->PhotoEtat;
    }

    public function addPhotoEtat(Etat $photoEtat): self
    {
        if (!$this->PhotoEtat->contains($photoEtat)) {
            $this->PhotoEtat[] = $photoEtat;
        }

        return $this;
    }

    public function removePhotoEtat(Etat $photoEtat): self
    {
        if ($this->PhotoEtat->contains($photoEtat)) {
            $this->PhotoEtat->removeElement($photoEtat);
        }

        return $this;
    }

    /**
     * @return Collection|Terrariophile[]
     */
    public function getPhotoTerrariophile(): Collection
    {
        return $this->PhotoTerrariophile;
    }

    public function addPhotoTerrariophile(Terrariophile $photoTerrariophile): self
    {
        if (!$this->PhotoTerrariophile->contains($photoTerrariophile)) {
            $this->PhotoTerrariophile[] = $photoTerrariophile;
        }

        return $this;
    }

    public function removePhotoTerrariophile(Terrariophile $photoTerrariophile): self
    {
        if ($this->PhotoTerrariophile->contains($photoTerrariophile)) {
            $this->PhotoTerrariophile->removeElement($photoTerrariophile);
        }

        return $this;
    }
    public function __toString()
    {
        // TODO: Implement __toString() method.
      return  $this->getPhoto();
    }
}
