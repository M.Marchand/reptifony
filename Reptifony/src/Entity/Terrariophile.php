<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TerrariophileRepository")
 */
class Terrariophile implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Telephone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Serpent", mappedBy="SerpentTerrariophile")
     */
    private $serpents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Photo", mappedBy="PhotoTerrariophile")
     */
    private $photos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Alertes", inversedBy="alerteTerrariophile")
     */
    private $alertes;

    public function __construct()
    {
        $this->serpents = new ArrayCollection();
        $this->photos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(?string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->Mail;
    }

    public function setMail(string $Mail): self
    {
        $this->Mail = $Mail;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->Telephone;
    }

    public function setTelephone(?string $Telephone): self
    {
        $this->Telephone = $Telephone;

        return $this;
    }

    /**
     * @return Collection|Serpent[]
     */
    public function getSerpents(): Collection
    {
        return $this->serpents;
    }

    public function addSerpent(Serpent $serpent): self
    {
        if (!$this->serpents->contains($serpent)) {
            $this->serpents[] = $serpent;
            $serpent->setSerpentTerrariophile($this);
        }

        return $this;
    }

    public function removeSerpent(Serpent $serpent): self
    {
        if ($this->serpents->contains($serpent)) {
            $this->serpents->removeElement($serpent);
            // set the owning side to null (unless already changed)
            if ($serpent->getSerpentTerrariophile() === $this) {
                $serpent->setSerpentTerrariophile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Photo[]
     */
    public function getPhotos(): Collection
    {
        return $this->photos;
    }

    public function addPhoto(Photo $photo): self
    {
        if (!$this->photos->contains($photo)) {
            $this->photos[] = $photo;
            $photo->addPhotoTerrariophile($this);
        }

        return $this;
    }

    public function removePhoto(Photo $photo): self
    {
        if ($this->photos->contains($photo)) {
            $this->photos->removeElement($photo);
            $photo->removePhotoTerrariophile($this);
        }

        return $this;
    }

    public function getAlertes(): ?Alertes
    {
        return $this->alertes;
    }

    public function setAlertes(?Alertes $alertes): self
    {
        $this->alertes = $alertes;

        return $this;
    }
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->mail;
    }

}
