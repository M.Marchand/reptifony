<?php

namespace App\Form;


use App\Entity\Photo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichFileType;

class embedHabitatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Hauteur')
            ->add('Longueur')
            ->add('Profondeur')
            ->add('Substrat')
            ->add('SystemeThermique')
            ->add('SystemeHygrometrique')

            ->add('PhotoHabitat')
            ->add('photoFile', VichFileType::class, [


            ])

            ->add('save',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Photo::class,
        ]);
    }
}
