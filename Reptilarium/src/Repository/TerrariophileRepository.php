<?php

namespace App\Repository;

use App\Entity\Terrariophile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Terrariophile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Terrariophile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Terrariophile[]    findAll()
 * @method Terrariophile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerrariophileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Terrariophile::class);
    }

    // /**
    //  * @return Terrariophile[] Returns an array of Terrariophile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Terrariophile
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
