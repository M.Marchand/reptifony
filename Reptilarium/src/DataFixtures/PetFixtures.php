<?php

namespace App\DataFixtures;

use App\Entity\Pet;
use App\DataFixtures\PersonFixtures;
use App\Entity\Serpent;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PetFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 20; $i++) { 
            $pet = new Serpent();
            $pet->setPrenom("Pet $i");
            $pet->setAge(mt_rand(0, 10));
            $pet->setOwner($this->getReference(PersonFixtures::PERSON_REFERENCE.$i));
            
            $manager->persist($pet);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PersonFixtures::class,
        );
    }
}
